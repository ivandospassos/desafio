//https://docs.google.com/spreadsheets/d/1I5gtjg4PNNzuTMbBkc1fDymfkY-XliUgtCMQIFzvcQ4/edit#gid=0

var GoogleSpreadsheet = require('google-spreadsheet');
var creds = require('./service_account.json');
var doc = new GoogleSpreadsheet('1I5gtjg4PNNzuTMbBkc1fDymfkY-XliUgtCMQIFzvcQ4');

doc.useServiceAccountAuth(creds, readFile);

/**
 * Lê um grupo de células do arquivo contendo informações sobre os alunos e executa as classificações
 */
function readFile(){
    doc.getCells(1 , {
        'min-row' : 4,
        'max-row' : 27,
        'min-col' : 3,
        'max-col' : 8,
        'return-empty' : true,
    }, (err, cells) => {
        for(i = 0; i < cells.length; i+=6){
            var [sit, final] = classification( parseInt(cells[i+1].value)
                                        , parseInt(cells[i+2].value)
                                        , parseInt(cells[i+3].value)
                                        , parseInt(cells[i].value)
                                    );
            final = ((parseInt(final) < final) ? (parseInt(final) + 1) : final);
            cells[i+4].setValue(String(sit), (err) => {
                //console.log(err);
            });
            cells[i+5].setValue(String(final), (err) => {
                //console.log(err);
            });
            console.log(cells[i+4].value + ':' + cells[i+5].value);
        }
    });
}

/**
 * 
 * Classificação dos alunos pela nota e frequência.
 * 
 * @param {number} p1 Primeira nota de 0 - 100
 * @param {number} p2 Segunda nota de 0 - 100
 * @param {number} p3 Terceira nota de 0 - 100
 * @param {number} freq Número de faltas
 * @returns {[string,number]} 
 */
function classification(p1,p2,p3,freq){
    var sit,                        //Situação do aluno
        score = ((p1+p2+p3) / 3 ),  //Média das notas
        final = 0;                  //Nota final
    if(freq/60 > 0.25){
        sit = 'Reprovado por falta';
    }else{
        if(score < 50) {
            sit = 'Reprovado por Nota';
        } else if(score < 70){
            final = 100 - score;
            sit = 'Exame Final';
        } else {
            sit = 'Aprovado';
        }
    }
    return [sit,final];
}